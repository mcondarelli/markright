#!/usr/bin/env python

from markright import *

from os import path
import argparse
import logging
logging.basicConfig(level=logging.INFO)


ap = argparse.ArgumentParser(description='Process a Markright document to produce publication.')

ap.add_argument('FILE', help='Markright file to process.')
ap.add_argument('-d', '--debug', help='Verbose debug output (not generally useful).',
                action='store_const', const=logging.DEBUG, default=False)
ap.add_argument('-o', '--out', help='output file (verbatim); if not given it is inferred from input fileame.',
                action='store', default=None)

og = ap.add_mutually_exclusive_group(required=True)
og.add_argument('--to-html', help='Produce single-file HTML output.',
                action='store_true', default=False)
og.add_argument('--to-docx', help='Produce MSWord/OpenOffice/LibreOffice compatible .docx output.',
                action='store_true', default=False)
og.add_argument('--to-epub', help='Produce ePUB output.',
                action='store_true', default=False)
og.add_argument('--to-latex', help='Produce LaTeX output.',
                action='store_true', default=False)
og.add_argument('-E', '--edit', help='Start editor.',
                action='store_true', default=False)
og.add_argument('-T', '--test', help="Testing framework (don't use!).",
                action='store_true', default=False)

ap.add_argument('-t', '--title', help='Publication title (string).',
                action='store', default=None)
ap.add_argument('--subtitle', help='Publication subtitle (string).',
                action='store', default=None)
ap.add_argument('-a', '--author', help='Author name.',
                action='store', default=None)
ap.add_argument('-s', '--css', '--style', help='Add custom CSS file (should be a filename).',
                action='store', default=None)
ap.add_argument('-i', '--include', help='Additional directory for images/fonts/css (should be a directory name).',
                action='store', default=None)
ap.add_argument('-f', '--font', help='Custom font to embed (filename); can be given multiple times.',
                action='append')

ar = ap.parse_args()

with open(ar.FILE, 'r') as fi:
    frag = fi.read()
r = parse(frag, debug=ar.debug)

kwargs = {}

if ar.title:
    kwargs['title'] = ar.title

if ar.subtitle:
    kwargs['subtitle'] = ar.subtitle

if ar.css:
    kwargs['__css'] = ar.css

kwargs['__inc'] = ar.include if ar.include else path.dirname(ar.FILE)

if ar.font:
    kwargs['__fts'] = ar.font

if ar.author:
    kwargs['author'] = ar.author


def _guess_out(ext):
    if ar.out is None:
        fname, _ = path.splitext(ar.FILE)
        fname += ext
    else:
        fname = ar.out
    kwargs['__out'] = fname


if ar.to_html:
    e = HTMLemitter()
    _guess_out('.html')
    e.to_html(r, **kwargs)

if ar.to_docx:
    e = DOCXemitter()
    d = e.emit(r, **kwargs)
    _guess_out('.docx')
    d.save(kwargs['__out'])


if ar.to_epub:
    e = EPUBemitter()
    _guess_out('.new.epub')  # FIXME: for testing. Should be: '.epub'
    d = e.to_epub(r, **kwargs)


if ar.to_latex:
    raise NotImplementedError()

if ar.edit:
    from mreditor.main_window import start
    out = ar.out if ar.out else ar.FILE
    start(r, out)

if ar.test:
    from mreditor.marshal import start
    # from mreditor.merge_metadata import start
    start(r)
