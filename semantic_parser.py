#!/usr/bin/env python

from zipfile import ZipFile
from lxml import etree, objectify
from os import path

# import logging
# import inspect
#
#
# class IndentFormatter(logging.Formatter):
#
#     def __init__(self, fmt=None, datefmt=None):
#         logging.Formatter.__init__(self, fmt, datefmt)
#         self.baseline = len(inspect.stack())
#
#     def format(self, rec):
#         stack = inspect.stack()
#         rec.indent = '  '*(len(stack)-self.baseline)
#         rec.function = stack[8][3]
#         out = logging.Formatter.format(self, rec)
#         del rec.indent
#         del rec.function
#         return out
#
#     @staticmethod
#     def getlogger(name):
#         formatter = IndentFormatter("[%(levelname)s]%(indent)s%(function)s:%(message)s")
#
#         logger = logging.getLogger(name)
#         handler = logging.StreamHandler()
#         handler.setFormatter(formatter)
#         logger.addHandler(handler)
#         logger.setLevel(logging.DEBUG)
#
#         return logger
#
#
# log = IndentFormatter.getlogger('parser')


class Handler:

    def __init__(self):
        self.output = ''
        self.head = ''
        self.tail = ''

        self._select = {
            'h1': self._handle_h1,
            'h2': self._handle_h2,
            'h3': self._handle_h3,
            'h4': self._handle_h4,
            'h5': self._handle_h5,
            'p': self._handle_p,
            'br': self._handle_br,
            'img': self._handle_img,
            'div': self._handle_div,
            'blockquote': self._handle_blockquote,
            'span': self._handle_span,
            'b': self._handle_b,
            'i': self._handle_i,
            'body': self._handle_body,
            'html': self._handle_html,
        }

    def close(self):
        out = self.output + self.head
        self.output = None                   # this is to force exception if this Handler is reused
        # log.debug(f'[{out}]')
        return out

    def _handle_h1(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@part{{{text}}}\n'

    def _handle_h2(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@chapter{{{text}}}\n'

    def _handle_h3(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@scene{{{text}}}\n'

    def _handle_h4(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@h4{{{text}}}\n'

    def _handle_h5(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@h5{{{text}}}\n'

    def _handle_p(self, e):
        output = self._recurse(e)
        self.output += f'{self.head}{output}\n\n'

    # noinspection PyUnusedLocal
    def _handle_br(self, e):
        self.output += self.head + '\n'

    def _handle_img(self, e):
        clazz = e.get('class', '')
        if clazz == 'stars':
            style = e.get('style', '')
            if style:
                style = '[' + style + ']'
            self.output += f'{self.head}@sep[stars]{style}{{{e.get("src")}}}\n'
        else:
            self.output += f'{self.head}@img[{e.get("class", "")}]{{{e.get("src")}}}'

    def _handle_div(self, e):
        style = e.get('style', '')
        if len(style) > 0:
            style = '[' + style + ']'
        clazz = e.get('class', '')
        output = self._recurse(e)
        if clazz == 'scene':
            self.output += f'{self.head}@scene{style}{{}}\n{output}'
        else:
            self.output += f'{self.head}@div[{clazz}]{style}{{{output}}}'

    def _handle_blockquote(self, e):
        style = e.get('style', '')
        clazz = e.get('class', '')
        clazz = f'[{clazz}]' if clazz or style else ''
        style = f'[{style}]' if style else ''
        output = self._recurse(e)
        self.output += f'{self.head}@quot{clazz}{style}{{{output}}}\n'

    def _handle_span(self, e):
        clazz = e.get('class', '')
        output = self._recurse(e)
        if clazz.startswith('speech'):
            t = ''
            i = self.head.rfind('«')
            if i >= 0:
                t = self.head[i+1:].strip()
                self.head = self.head[:i]
            else:
                i = self.head.rfind('“')
                if i >= 0:
                    t = self.head[i+1:].strip()
                    self.head = self.head[:i]
            i = self.tail.find('»')
            if i >= 0:
                self.tail = self.tail[i+1:]
            else:
                i = self.tail.find('”')
                if i >= 0:
                    self.tail = self.tail[i+1:]
            self.output += f'{self.head}@speech[{clazz}]{{{t}{output}}}'
        else:
            self.output += f'{self.head}@span[{clazz}]{{{output}}}'

    def _handle_b(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@stress{{{text}}}'

    def _handle_i(self, e):
        text = ''.join(e.itertext())
        self.output += f'{self.head}@standout{{{text}}}'

    def _handle_body(self, e):
        output = self._recurse(e)
        # self.output += f'@body\n{output}\n@ydob'
        self.output += f'\n{output}\n'

    def _handle_html(self, e):
        output = self._recurse(e)
        # self.output += f'@body\n{output}\n@ydob'
        if self.output:
            print('non-empty output in _handle_html')
        self.output += output

    def _ignore(self, e):
        print(f'ignoring tag "{e.tag}"')

    def dispatch(self, e):
        t = e.tag
        f = self._select.get(t, self._ignore)
        # log.debug(f'>> ([{self.head!r}] {t} [{self.tail!r}]): "{self.output!r}"')
        f(e)
        # log.debug(f'<< ([{self.head!r}] {t} [{self.tail!r}]): "{self.output!r}"')

    @staticmethod
    def _recurse(e):
        h = Handler()
        h.head = e. text if e.text is not None else ''
        # log.debug(f'>> ({e.tag}): [{h.head!r}]')
        for c in e.iterchildren():
            h.tail = c.tail if c.tail is not None else ''
            h.dispatch(c)
            h.head = h.tail
        out = h.close()
        # log.debug(f'<< [{out}]')
        return out


def handle_html(html):
    r = ''
    s = etree.tostring(html)
    print(f'\n{s.decode()}\n----------------------------------------------------------------------------------')
    for body in html.iter('body'):
        h = Handler()
        h.dispatch(body)
        s = h.close()
        print(s)
        r += s
    print(f'\n==================================================================================')
    return r


def suck_xml(stream):
    parser = etree.XMLParser(ns_clean=True,
                             no_network=True,
                             remove_comments=True,
                             remove_pis=True,
                             remove_blank_text=True)
    tree = etree.parse(stream, parser)
    root = tree.getroot()
    for elem in root.getiterator():
        if hasattr(elem.tag, 'find'):
            i = elem.tag.rfind('}')
            if i >= 0:
                elem.tag = elem.tag[i+1:]
    objectify.deannotate(root, cleanup_namespaces=True)
    return root


def handle_zip(name):
    outfn = path.splitext(name)[0] + '.markright'
    with ZipFile(name, 'r') as zr:
        with zr.open('META-INF/container.xml') as fi:
            container = suck_xml(fi)
        with open(outfn, 'w') as outf:
            root_files = container.xpath("/container/rootfiles/rootfile")
            for root_file in root_files:
                full_path = root_file.get('full-path')
                print(full_path)
                base = path.dirname(full_path)
                with zr.open(full_path) as fi:
                    contents = suck_xml(fi)
                itemrefs = contents.xpath('/package/spine/itemref')
                for itemref in itemrefs:
                    itid = itemref.get('idref')
                    item = contents.xpath(f'/package/manifest/item[@id="{itid}"]/@href')
                    item = path.join(base, item[0])
                    print(f'{itid} -> {item}')
                    if item.endswith('.html'):
                        with zr.open(item) as fi:
                            doc = suck_xml(fi)
                        out = handle_html(doc)
                        outf.write(f'\n@sep[file]{{{item}}}\n')
                        outf.write(out)
            outf.write(f'\n@sep[end]\n')


if __name__ == '__main__':
    from sys import argv
    handle_zip(argv[1])
