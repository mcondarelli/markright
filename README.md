This project will provide definition and implementation of a simple semantic markup language 
specifically oriented to fiction writing.

The usual section definition is supported (Book, Part, Chapter Scene) along with fancy nested 
Direct Speech, Image embedding, Font embedding and generic styling using CSS.


### Tags
Current (possibly incomplete) tag list include:

* @&#8203;part\[class=part]{Title}: Major book division.
* @&#8203;chapter\[class=chapter]{Title}: Chapter.
* @&#8203;scene\[class=scene]{Title=''}: Scene division; can generate visual break (e.g.: centered "* * *").
* @speech\[class=speech]{Direct speech fragment}: Direct speech, possibly nested; 
can contain visual enhancements.
* @&#8203;stress\[class]\[style]{stressed fragment}: Visually enhanced fragment (normally rendered as italic).
* @&#8203;standout\[class]\[style]{standout fragment}: Visually enhanced fragment (normally rendered as bold).
* @&#8203;img\[class]{url}: Inline image.
* @&#8203;quot\[class=quote]\[style]{Possibly long and structured block quote}: Block quote; normally rendered 
as indented italic block.
* @&#8203;sep\[class]{url}: special separator; currently defined are:
  * class="stars"; url="bullet image": scene separator.
  * class="page: page break.
  * class="end": end of input.
* @&#8203;div\[class]\[style]{div block}: Currently for internal use only.
* @&#8203;span\[class]\[style]{span block}: Currently for internal use only.

### Newline
Newline handling follows standard rule:
* Newlines are preserved (no automatic refill).
* Double newline indicates paragraph break.

By default paragraph breaks are rendered as 1.5 line spacing, no indent.

### Backends
All Back-ends will support automatic TOC generation and support for CSS, Image and Font embedding.
Output backends currently in the works include:
* EPub: Standard ePUB output for e-book viewers.
* MOBI: Amazon-style e-book format.
* HTML: Single-file \[x]HTML output for web reading.
* DOCX: Suitable for editing with MS-Word.
* LaTeX: Single-file LaTeX output for professional typesetting.

### Command-line interface
Current command line interface (to be expanded) is:

```
$ markright/markright.py -h
usage: markright.py [-h] [-d] [-o OUT]
                    (--to-html | --to-docx | --to-epub | --to-latex)
                    [-t TITLE] [--subtitle SUBTITLE] [-a AUTHOR] [-s CSS]
                    [-i INCLUDE] [-f FONT]
                    FILE

Process a Markright document to produce publication.

positional arguments:
  FILE                  Markright file to process.

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           Verbose debug output (not generally useful).
  -o OUT, --out OUT     output file (verbatim); if not given it is inferred
                        from input fileame.
  --to-html             Produce single-file HTML output.
  --to-docx             Produce MSWord/OpenOffice/LibreOffice compatible .docx
                        output.
  --to-epub             Produce ePUB output.
  --to-latex            Produce LaTeX output.
  -t TITLE, --title TITLE
                        Publication title (string).
  --subtitle SUBTITLE   Publication subtitle (string).
  -a AUTHOR, --author AUTHOR
                        Author name.
  -s CSS, --css CSS, --style CSS
                        Add custom CSS file (should be a filename).
  -i INCLUDE, --include INCLUDE
                        Additional directory for images/fonts/css (should be a
                        directory name).
  -f FONT, --font FONT  Custom font to embed (filename); can be given multiple
                        times.
```

### Parsers
Beside Markright parser a few other import programs are in the work to help bootstrapping development.

In particular `semantic-parser.py` will be able to parse EPub backend output so that a round trip is stable.
This enables doing small changes with other tools (e.g.: Sigil) and keep changes.

A similar tool will be provided for DOCX Backend to enable usage of extended Linguistic checks available
in MS-Word.

These tools are *NOT* intended to cover the whole range of capabilities provided by those external 
programs, but should be enough for round-trip with small editing.
### NOTE
All development is done in Python (3.7) under Linux; it *should* work in other environments.

If there will be enough interest I might consider a specialized editor (Qt).