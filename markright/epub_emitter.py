from .html_emitter import HTMLemitter

import mkepub

from os import path

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class EPUBemitter(HTMLemitter):
    def push(self, title, kwargs):
        try:
            parent = kwargs['__stk'][-1]
        except IndexError:
            parent = None
        fo = kwargs['epub'].add_page(title, parent=parent)
        kwargs['__stk'].append(fo)
        kwargs['__fo'] = fo
        return fo

    def pop(self, kwargs):
        retf = kwargs['__stk'].pop()
        if kwargs['__stk']:
            __fo = kwargs['__stk'][-1]
            kwargs['__fo'] = __fo
        else:
            del kwargs['__fo']
        return retf

    # divisions ========================

    def book(self, node, **kwargs):
        cover = kwargs.get('cover')
        if cover:
            with open(cover) as fi:
                data = fi.read()
            kwargs['epub'].set_cover(data)
        # TODO: handle .css
        t = kwargs.get('title')
        s = kwargs.get('subtitle')
        a = kwargs.get('author')
        fo = self.push(t, kwargs)
        fo.write(f'<div class="title">{t}</div>')
        if s:
            fo.write(f'<div class="subtitle">{s}</div>')
        if a:
            fo.write(f'<div class="author">{a}</div>')
        self.recurse(node, **kwargs)
        page = self.pop(kwargs)
        if page:
            page.close()

    def part(self, node, **kwargs):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '{} - ')
        fo = self.push(t, kwargs)
        fo.write(f'<h1{c}{i}>{n}{t}</h1>\n')
        self.recurse(node, **kwargs)
        fo.write('\n')
        page = self.pop(kwargs)
        if page:
            page.close()

    def chapter(self, node, **kwargs):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '{} - ')
        fo = self.push(t, kwargs)
        fo.write(f'<h2{c}{i}>{n}{t}</h2>\n')
        self.recurse(node, **kwargs)
        fo.write('\n')
        page = self.pop(kwargs)
        if page:
            page.close()

    # scene are not on a separate page
    # def scene(self, node, **kwargs):
    #     c = node.render_field('clazz')
    #     t = node.render_field('title')
    #     i = node.render_field('ref')
    #     n = node.render_field('seq')
    #     super(EPUBemitter, self).scene(**kwargs)

    # ignore non-stars separators
    def sep(self, node, **kwargs):
        c = node.render_field('clazz')
        t = node.render_field('url')
        fo = kwargs['__fo']
        if c == 'stars':
            fo.write(f'<p class="{c}"><img alt="* * *" class="{c}" src="{t}"/></p>')
            fn = t if path.isfile(t) else path.join(kwargs.get('__inc', '.'), t)
            if path.isfile(fn):
                with open(fn, 'rb') as fi:
                    data = fi.read()
                u = kwargs['epub'].add_image(path.basename(t), data)
                kwargs['__fo'].write(f'<img{c} src="{u}"/>')

    # COMMANDS =========================

    def img(self, node, **kwargs):
        clazz = node.render_field('clazz', ' class="{}"')
        url = node.render_field('url')
        fn = url if path.isfile(url) else path.join(kwargs.get('__inc', '.'), url)
        if path.isfile(fn):
            with open(fn, 'rb') as fi:
                data = fi.read()
            u = kwargs['epub'].add_image(path.basename(url), data)
            kwargs['__fo'].write(f'<img{clazz} src="{u}"/>')

    # DRIVER ===========================

    def to_epub(self, node, **kwargs):
        import os
        log.debug('CWD:"%s"', os.getcwd())

        fname = kwargs.get('__out', 'out.epub')
        title = kwargs.get('title')
        if not title:
            title, _ = path.splitext(path.basename(fname))
            kwargs['title'] = title
        epub = mkepub.Book(**kwargs)
        kwargs['epub'] = epub

        inc = kwargs.get('__inc', '.')
        # ===== CSS =====
        css = kwargs.get('__css')
        if css:
            fn = css if path.isfile(css) else path.join(inc, css)
            if path.isfile(fn):
                with open(fn) as fi:
                    data = fi.read()
                epub.set_stylesheet(data)
            else:
                log.warning('Unable to find CSS in %s', css)

        # ===== FONTs =====
        fonts = kwargs.get('__fts')
        for font in fonts:
            fn = font if path.isfile(font) else path.join(inc, font)
            if path.isfile(fn):
                with open(fn, 'br') as fi:
                    data = fi.read()
                epub.add_font(path.basename(font), data)
            else:
                log.warning('Unable to find FONT in %s', font)

        # ===== CONTENT =====
        kwargs['__stk'] = []
        super(EPUBemitter, self).emit(node, **kwargs)
        epub.save(fname)
