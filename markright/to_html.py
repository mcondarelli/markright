import logging
log = logging.getLogger(__name__)


def class_to_html(node, _toks, **_):
    return f'class="{node.render()}"'


def url_to_html(node, _toks, **_):
    return node.render()


def title_to_html(node, _toks, **_):
    s = node.render_field("seq", "{} - ")
    return f'{s}{node.render()}'


def style_to_html(node, _toks, **_):
    return node.render_field('style', 'style="{}"')


def fragn_to_html(_node, _toks, **_):
    raise NotImplementedError('Fragn should never be rendered')


def fragl_to_html(_node, _toks, **_):
    raise NotImplementedError('Fragl should never be rendered')


def fragp_to_html(_node, _toks, **_):
    raise NotImplementedError('Fragp should never be rendered')


def frag_to_html(_node, _toks, **_):
    raise NotImplementedError('Frag should never be rendered')


def content_to_html(_node, toks, **_):
    s = ''.join(toks)
    return s


def para_to_html(_node, toks, **_):
    s = ''.join(toks)
    return f'<p>{s}</p>\n'


def newline_to_html(node, _toks, **_):
    return '<br/>\n' if not node.para else ''


def text_to_html(node, _toks, **_):
    return node.text


# divisions ========================


def book_to_html(_node, toks, **_):
    s = ''.join(toks)
    h = """\
<head>
<style>
.stars {
    text-align: center;
}
</style>
</head>
"""
    r = f'<body>\n{s}\n</body>\n'
    log.debug(r)
    return '<html>\n' + h + r + '</html>\n'


def part_to_html(node, toks, **_):
    c = node.render_field('clazz', ' class="{}"')
    t = node.render_field('title')
    i = node.render_field('ref', ' id="{}"')
    n = node.render_field('seq', '{} - ')
    s = ''.join(toks)
    r = f'<h1{c}{i}>{n}{t}</h1>\n{s}\n'
    log.debug(r)
    return r


def chapter_to_html(node, toks, **_):
    c = node.render_field('clazz', ' class="{}"')
    t = node.render_field('title')
    i = node.render_field('ref', 'id="{}"')
    n = node.render_field('seq', '{} - ')
    s = ''.join(toks)
    r = f'<h2{c}{i}>{n}{t}</h1>\n{s}\n'
    log.debug(r)
    return r


def scene_to_html(node, toks, **_):
    c = node.render_field('clazz', ' class="{}"')
    t = node.render_field('title', '<!-- title: {} -->')
    i = node.render_field('ref', ' id="{}"')
    n = node.render_field('seq', '<!-- sequence: {} -->')
    s = ''.join(toks)
    r = f'<div{c}{i}>{n}{t}\n{s}</div>'
    log.debug(r)
    return r


def sep_to_html(node, _toks, **_):
    c = node.render_field('clazz')
    t = node.render_field('url')
    if c == 'stars':
        r = f'<p class="{c}"><img alt="* * *" class="{c}" src="{t}"/></p>'
    else:
        r = f'<!-- {c}:{t} -->\n'
    log.debug(r)
    return r


# commands =========================


def speech_to_html(node, toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    s = ''.join(toks)
    r = f'« <span{clazz}>{s}</span> »'
    log.debug(r)
    return r


def stress_to_html(node, toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    s = ''.join(toks)
    r = f'<b{clazz}>{s}</b>'
    log.debug(r)
    return r


def standout_to_html(node, toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    s = ''.join(toks)
    r = f'<i{clazz}>{s}</i>'
    log.debug(r)
    return r


def quot_to_html(node, toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    style = node.render_field('style', ' style="{}"')
    s = ''.join(toks)
    r = f'<i{clazz}{style}>{s}</i>'
    log.debug(r)
    return r


def img_to_html(node, _toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    url = node.render_field('url', ' src="{}"')
    r = f'<img{clazz}{url}/>'
    log.debug(r)
    return r


def div_to_html(node, toks, **_):
    clazz = node.render_field('clazz', ' class="{}"')
    style = node.render_field('style', ' style="{}"')
    s = ''.join(toks)
    r = f'<div{clazz}{style}>{s}</div>'
    log.debug(r)
    return r


handlers = {
    'Class': class_to_html,
    'Url': url_to_html,
    'Title': title_to_html,
    'Style': style_to_html,
    'Fragn': fragn_to_html,
    'Fragl': fragl_to_html,
    'Fragp': fragp_to_html,
    'Frag': frag_to_html,
    'Content': content_to_html,
    'Para': para_to_html,
    'NewLine': newline_to_html,
    'Text': text_to_html,
    'Book': book_to_html,
    'Part': part_to_html,
    'Chapter': chapter_to_html,
    'Scene': scene_to_html,
    'Sep': sep_to_html,
    'Speech': speech_to_html,
    'Stress': stress_to_html,
    'Standout': standout_to_html,
    'Quot': quot_to_html,
    'Img': img_to_html,
    'Div': div_to_html,
    }


def to_html(node):
    return node.walk(handlers)
