from abc import ABC, abstractmethod


class Emitter(ABC):

    # @abstractmethod
    # def clazz(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def url(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def title(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def style(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def fragn(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def fragl(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def fragp(self, node, **kwargs):
    #     pass
    #
    # @abstractmethod
    # def frag(self, node, **kwargs):
    #     pass

    @abstractmethod
    def content(self, node, **kwargs):
        pass

    @abstractmethod
    def para(self, node, **kwargs):
        pass

    @abstractmethod
    def newline(self, node, **kwargs):
        pass

    @abstractmethod
    def text(self, node, **kwargs):
        pass

    # divisions ========================

    @abstractmethod
    def book(self, node, **kwargs):
        pass

    @abstractmethod
    def part(self, node, **kwargs):
        pass

    @abstractmethod
    def chapter(self, node, **kwargs):
        pass

    @abstractmethod
    def scene(self, node, **kwargs):
        pass

    @abstractmethod
    def sep(self, node, **kwargs):
        pass
    
    # commands =========================

    @abstractmethod
    def speech(self, node, **kwargs):
        pass

    @abstractmethod
    def stress(self, node, **kwargs):
        pass

    @abstractmethod
    def standout(self, node, **kwargs):
        pass

    @abstractmethod
    def quot(self, node, **kwargs):
        pass

    @abstractmethod
    def img(self, node, **kwargs):
        pass

    @abstractmethod
    def div(self, node, **kwargs):
        pass

    @abstractmethod
    def meta(self, node, **kwargs):
        pass

    def __init__(self):
        self.handlers = {
            # 'Class': self.clazz,
            # 'Url': self.url,
            # 'Title': self.title,
            # 'Style': self.style,
            # 'Fragn': self.fragn,
            # 'Fragl': self.fragl,
            # 'Fragp': self.fragp,
            # 'Frag': self.frag,
            'Content': self.content,
            'Para': self.para,
            'NewLine': self.newline,
            'Text': self.text,
            'Book': self.book,
            'Part': self.part,
            'Chapter': self.chapter,
            'Scene': self.scene,
            'Sep': self.sep,
            'Speech': self.speech,
            'Stress': self.stress,
            'Standout': self.standout,
            'Quot': self.quot,
            'Img': self.img,
            'Div': self.div,
            'Meta': self.meta,
            }

    # DRIVER ===========================

    def emit(self, node, **kwargs):
        func = self.handlers.get(type(node).__name__)
        if func:
            func(node, **kwargs)
    
    def recurse(self, node, **kwargs):
        for c in node.children:
            self.emit(c, **kwargs)
