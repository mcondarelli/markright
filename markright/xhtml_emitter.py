from .emitter import Emitter
import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class HTMLemitter(Emitter):

    def content(self, _node, toks, **_):
        s = ''.join(toks)
        return s

    def para(self, _node, toks, **_):
        s = ''.join(toks)
        return f'<p>{s}</p>\n'

    def newline(self, node, _toks, **_):
        return '<br/>\n' if not node.para else ''

    def text(self, node, _toks, **_):
        return node.text

    # divisions ========================

    def book(self, _node, toks, **_):
        s = ''.join(toks)
        h = """\
<head>
<style>
.stars {
    text-align: center;
}
</style>
</head>
"""
        r = f'<body>\n{s}\n</body>\n'
        log.debug(r)
        return '<html>\n' + h + r + '</html>\n'

    def part(self, node, toks, **_):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '{} - ')
        s = ''.join(toks)
        r = f'<h1{c}{i}>{n}{t}</h1>\n{s}\n'
        log.debug(r)
        return r

    def chapter(self, node, toks, **_):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', 'id="{}"')
        n = node.render_field('seq', '{} - ')
        s = ''.join(toks)
        r = f'<h2{c}{i}>{n}{t}</h1>\n{s}\n'
        log.debug(r)
        return r

    def scene(self, node, toks, **_):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title', '<!-- title: {} -->')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '<!-- sequence: {} -->')
        s = ''.join(toks)
        r = f'<div{c}{i}>{n}{t}\n{s}</div>'
        log.debug(r)
        return r

    def sep(self, node, _toks, **_):
        c = node.render_field('clazz')
        t = node.render_field('url')
        if c == 'stars':
            r = f'<p class="{c}"><img alt="* * *" class="{c}" src="{t}"/></p>'
        else:
            r = f'<!-- {c}:{t} -->\n'
        log.debug(r)
        return r

    # commands =========================

    def speech(self, node, toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        s = ''.join(toks)
        r = f'« <span{clazz}>{s}</span> »'
        log.debug(r)
        return r

    def stress(self, node, toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        s = ''.join(toks)
        r = f'<b{clazz}>{s}</b>'
        log.debug(r)
        return r

    def standout(self, node, toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        s = ''.join(toks)
        r = f'<i{clazz}>{s}</i>'
        log.debug(r)
        return r

    def quot(self, node, toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        style = node.render_field('style', ' style="{}"')
        s = ''.join(toks)
        r = f'<i{clazz}{style}>{s}</i>'
        log.debug(r)
        return r

    def img(self, node, _toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        url = node.render_field('url', ' src="{}"')
        r = f'<img{clazz}{url}/>'
        log.debug(r)
        return r

    def div(self, node, toks, **_):
        clazz = node.render_field('clazz', ' class="{}"')
        style = node.render_field('style', ' style="{}"')
        s = ''.join(toks)
        r = f'<div{clazz}{style}>{s}</div>'
        log.debug(r)
        return r
