from .emitter import Emitter
import docx

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class DOCXemitter(Emitter):

    def content(self, node, doc=None):
        self.recurse(node, doc=doc)

    def para(self, node, doc=None):
        doc.add_paragraph()
        self.recurse(node, doc=doc)

    def newline(self, node, doc=None):
        p = doc.paragraphs[-1]
        p.add_run('\n')

    def text(self, node, doc=None):
        p = doc.paragraphs[-1]
        p.add_run(node.text)

    # divisions ========================

    def book(self, node, doc=None):
        self.recurse(node, doc=doc)

    def part(self, node, doc=None):
        doc.add_heading(node.render_field('title'), level=1)
        doc.add_paragraph()
        self.recurse(node, doc=doc)

    def chapter(self, node, doc=None):
        doc.add_heading(node.render_field('title'), level=2)
        doc.add_paragraph()
        self.recurse(node, doc=doc)

    def scene(self, node, doc=None):
        doc.add_heading(node.render_field('title'), level=3)
        doc.add_paragraph()
        self.recurse(node, doc=doc)

    def sep(self, node, _doc=None):
        pass  # FIXME: what should we do?

    # commands =========================

    def speech(self, node, doc=None):  # FIXME: this is UGLY!
        _open = '«“"""""""'
        close = '»”"""""""'
        p = doc.paragraphs[-1]
        p.add_run(f' {_open[self.speech_level]}\u202f')
        self.speech_level += 1
        self.recurse(node, doc=doc)
        self.speech_level -= 1
        p.add_run(f'\u202f{close[self.speech_level]}')

    def stress(self, node, doc=None):
        # TODO: not handled, for now
        self.recurse(node, doc=doc)

    def standout(self, node, doc=None):
        # TODO: not handled, for now
        self.recurse(node, doc=doc)

    def quot(self, node, doc=None):
        styl = doc.styles['Quote']
        para = doc.paragraphs[-1]
        old = para.style
        para.style = styl
        self.recurse(node, doc=doc)
        para = doc.paragraphs[-1]
        para.style = old

    def img(self, node, _doc=None):
        # TODO: not handled, for now
        pass

    def div(self, node, doc=None):
        # TODO: not handled, for now
        self.recurse(node, doc=doc)

    def __init__(self):
        super(DOCXemitter, self).__init__()
        self.speech_level = 0

    def emit(self, node, **kwargs):
        doc = docx.Document()
        doc.add_paragraph()
        kwargs['doc'] = doc
        super(DOCXemitter, self).emit(node, **kwargs)
        return doc
