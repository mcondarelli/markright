from pyparsing import *

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

ParserElement.setDefaultWhitespaceChars(' \t')


class ASTnode(object):

    def __init__(self):
        self.parent = None
        self.container = None
        self.children = []

    def __str__(self):
        return self.__class__.__name__ + ':' + str(self.__dict__)
    __repr__ = __str__

    def render(self):
        return type(self).__name__

    def render_children(self):
        li = []
        for c in self.children:
            li.append(f'<{type(c).__name__}>')
        return '[[' + ' '.join(li) + ']]'

    def render_field(self, name, fmt='{}'):
        try:
            f = getattr(self, name)
            if isinstance(f, ASTnode):
                f = f.render()
            if not f:
                raise AttributeError()
            return fmt.format(f)
        except AttributeError:
            return ''

    def dump(self, indent='  ', prefix=''):
        print(f'{prefix}{self.render()}')
        for c in self.children:
            c.dump(indent, indent+prefix)

    def walk(self, handlers: dict, **kwargs):
        toks = [c.walk(handlers, **kwargs) for c in self.children]
        func = handlers.get(type(self).__name__)
        if func is not None:
            return func(self, toks, **kwargs)
        else:
            return toks


class Class(ASTnode):
    def __init__(self, toks):
        super(Class, self).__init__()
        self.classes = toks

    def render(self):
        if self.classes:
            return " ".join(self.classes)
        else:
            return None


class Url(ASTnode):
    def __init__(self, toks):
        super(Url, self).__init__()
        self.url = toks[0]

    def render(self):
        return self.url


class Title(ASTnode):
    def __init__(self, toks):
        super(Title, self).__init__()
        try:
            while len(toks) > 0:
                _ = int(toks[0])
                toks.pop(0)
        except ValueError:
            if toks[0] == '-':  # FIXME: this is needed to counter default style (see _Parser.handle_chapter)
                toks.pop(0)
        self.title = ' '.join(toks)

    def render(self):
        return self.title


class Style(ASTnode):
    def __init__(self, toks):
        super(Style, self).__init__()
        self.style = ' '.join(toks)

    def render(self):
        return self.style


class Fragn(ASTnode):
    def __init__(self, toks):
        super(Fragn, self).__init__()
        for t in toks:
            self.children.append(t)

    def render(self):
        raise NotImplementedError('Fragn should never be rendered')


class Fragl(ASTnode):
    def __init__(self, toks):
        super(Fragl, self).__init__()
        fl = toks[0].children
        self.children.extend(fl)
        self.children.append(NewLine())

    def render(self):
        raise NotImplementedError('Fragl should never be rendered')


class Fragp(ASTnode):
    def __init__(self, toks):
        super(Fragp, self).__init__()
        fl = toks[0].children
        self.children.extend(fl)
        self.children.append(NewLine(True))

    def render(self):
        raise NotImplementedError('Fragp should never be rendered')


class Frag(ASTnode):
    def __init__(self, toks):
        super(Frag, self).__init__()
        for t in toks:
            if len(t.children) == 0:
                self.children.append(t)
            else:
                self.children.extend(t.children)

    def reformat(self):
        found = False
        li = []
        out = []
        for c in self.children:
            try:
                if not c.para:
                    raise AttributeError()
                pa = Para()
                pa.children.extend(li)
                li = []
                out.append(pa)
                found = True
            except AttributeError:
                li.append(c)
        if len(li) > 0:
            if found:
                pa = Para()
                pa.children.extend(li)
                out.append(pa)
            else:
                out.extend(li)
        return out

    def render(self):
        raise NotImplementedError('Frag should never be rendered')


class Content(ASTnode):
    def __init__(self, cont):
        super(Content, self).__init__()
        self.para = False
        li = []
        out = []
        for c in cont.children:
            try:
                if not c.para:
                    raise AttributeError()
                pa = Para()
                pa.children.extend(li)
                li = []
                out.append(pa)
                self.para = True
            except AttributeError:
                li.append(c)
        if len(li) > 0:
            if self.para:
                pa = Para()
                pa.children.extend(li)
                out.append(pa)
            else:
                out.extend(li)
        self.children = out

    def render(self):
        li = [r.render() for r in self.children]
        return '\n'.join(li)


class Para(ASTnode):
    def __init__(self):
        super(Para, self).__init__()
        self.para = True

    def render(self):
        return '@@paragraph@@'


class NewLine(ASTnode):
    def __init__(self, para=False):
        super(NewLine, self).__init__()
        self.para = para

    def render(self):
        return '@@newpara@@' if self.para else '@@newline@@'


class Text(ASTnode):
    def __init__(self, toks):
        super(Text, self).__init__()
        self.text = toks[0]

    def render(self):
        return self.text


# divisions ========================


class Book(ASTnode):
    def __init__(self, toks):
        super(Book, self).__init__()
        for c in toks:
            try:
                self.children.extend(c.reformat())
            except AttributeError:
                self.children.append(c)
        log.debug(len(self.children))


class Part(ASTnode):
    def __init__(self, toks):
        super(Part, self).__init__()
        self.clazz = toks.get('clazz', 'part')
        self.title = toks['title']

    def render(self):
        return f'Part[{self.clazz}] "{self.title.render()}"'


class Chapter(ASTnode):
    def __init__(self, toks):
        super(Chapter, self).__init__()
        self.clazz = toks.get('clazz', 'chapter')
        self.title = toks['title']

    def render(self):
        return f'Chapter[{self.clazz}] "{self.title.render()}"'


class Scene(ASTnode):
    def __init__(self, toks):
        super(Scene, self).__init__()
        self.clazz = toks.get('clazz', 'scene')
        self.title = toks['title']

    def render(self):
        return f'Scene[{self.clazz}] "{self.title.render()}"'


class Sep(ASTnode):
    def __init__(self, toks):
        super(Sep, self).__init__()
        self.clazz = toks.get('clazz', '')
        self.url = toks.get('url', '')

    def render(self):
        return f'Sep[{self.render_field("clazz")}]{{{self.render_field("url")}}}'


# commands =========================


class Speech(ASTnode):
    def __init__(self, toks):
        super(Speech, self).__init__()
        self.clazz = toks.get('clazz')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz', '[{}]')  # FIXME:!!!!!
        return f'Speech{c}{{{self.render_children()}}}'


class Stress(ASTnode):
    def __init__(self, toks):
        super(Stress, self).__init__()
        self.clazz = toks.get('clazz')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz', '[{}]')
        return f'Stress{c}{{{self.render_children()}}}'


class Standout(ASTnode):
    def __init__(self, toks):
        super(Standout, self).__init__()
        self.clazz = toks.get('clazz')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz', '[{}]')
        return f'Standout{c}{{{self.render_children()}}}'


class Quot(ASTnode):
    def __init__(self, toks):
        super(Quot, self).__init__()
        self.clazz = toks.get('clazz')
        self.style = toks.get('style')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz')
        s = self.render_field('style', '[{}]')
        c = f'[{c}]' if c or s else ''
        return f'Quot{c}{s}{{{self.render_children()}}}'


class Img(ASTnode):
    def __init__(self, toks):
        super(Img, self).__init__()
        self.clazz = toks.get('clazz')
        self.url = toks['url']

    def render(self):
        c = self.render_field('clazz', '[{}]')
        u = self.render_field('url')
        return f'Img{c}{{{u}}}'


class Div(ASTnode):
    def __init__(self, toks):
        super(Div, self).__init__()
        self.clazz = toks.get('clazz')
        self.style = toks.get('style')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz')
        s = self.render_field('style', '[{}]')
        c = f'[{c}]' if c or s else ''
        return f'Div{c}{s}{{{self.render_children()}}}'


class Meta(ASTnode):
    def __init__(self, toks):
        super(Meta, self).__init__()
        self.clazz = toks.get('clazz')
        self.children.append(Content(toks['content']))

    def render(self):
        c = self.render_field('clazz', '[{}]')
        return f'Div{c}{{{self.render_children()}}}'


# GRAMMAR =================================================


nl = Regex(r'\n(?!\n)').suppress()
ps = Regex(r'\n{2,}').suppress()
bl = Regex(r'\s*').suppress()

lbracket = Literal('[').suppress()
rbracket = Literal(']').suppress()
lbrace = Literal('{').suppress()
rbrace = Literal('}').suppress()

clazz = lbracket + ZeroOrMore(Word(alphas+'-') + Optional(':' + Word(alphanums))) + rbracket
clazz.setParseAction(Class)

url = lbrace + Combine(OneOrMore(Word(alphanums+'-_.@') | '/' | ':')) + rbrace
url.setParseAction(Url)

# title = lbrace + ZeroOrMore(Word(alphanums+"-_.'!")) + rbrace
title = lbrace + Regex(r'[^}]*') + rbrace
title.setParseAction(Title)

text = Regex(r'[^@}\n]+').setResultsName('frag')
text.setParseAction(Text)

style = lbracket + \
        ZeroOrMore(
            (Word(alphas+'-') + ':' + Word(printables+' ', excludeChars=';') + ';')
            | nl).setResultsName('style') + \
        rbracket
style.setParseAction(Style)

frag = Forward()
frag.setParseAction(Frag)

sep = Literal('@sep').suppress() + Optional(clazz('clazz')) + Optional(url('url'))
sep.setParseAction(Sep)

img = Literal('@img').suppress() + Optional(clazz('clazz')) + url('url')
img.setParseAction(Img)

div = Literal('@div').suppress() + clazz('clazz') + Optional(style('style')) + lbrace + frag('content') + rbrace
div.setParseAction(Div)

meta = Literal('@meta').suppress() + clazz('clazz') + lbrace + frag('content') + rbrace
meta.setParseAction(Meta)


span = Literal('@span').suppress() + clazz('clazz') + lbrace + frag('content') + rbrace
span.setResultsName('span')


part = Literal("@part").suppress() + Optional(clazz('clazz')) + title('title')
part.setParseAction(Part)

chapter = Literal("@chapter") + Optional(clazz('clazz')) + title('title')
chapter.setParseAction(Chapter)

scene = Literal("@scene").suppress() + Optional(clazz('clazz')) + title('title')
scene.setParseAction(Scene)


speech = Literal("@speech").suppress() + Optional(clazz('clazz')) + lbrace + frag('content') + rbrace
speech.setParseAction(Speech)

stress = Literal("@stress").suppress() + Optional(clazz('clazz')) + lbrace + frag('content') + rbrace
stress.setParseAction(Stress)

standout = Literal("@standout").suppress() + Optional(clazz('clazz')) + lbrace + frag('content') + rbrace
standout.setParseAction(Standout)

quot = "@quot" + Optional(clazz('clazz')) + Optional(style('style')) \
       + lbrace + frag.setResultsName('content') + rbrace
quot.setParseAction(Quot)

fragn = OneOrMore(div('div')
                  | img('img')
                  | speech('speech')
                  | stress('stress')
                  | standout('standout')
                  | span('span')
                  | text('text'))
fragn.setResultsName('fragn')
fragn.setParseAction(Fragn)

fragl = fragn('frag') + nl
fragl.setParseAction(Fragl)

fragp = fragn('frag') + ps
fragp.setParseAction(Fragp)

frag <<= (bl + OneOrMore(fragp('frag') | fragl('frag') | fragn('frag')))


book = OneOrMore(part('part')
                 | chapter('chapter')
                 | scene('scene')
                 | sep('sep')
                 | meta('meta')
                 | frag('frag')
                 | quot('quot')
                 | Suppress('\n'))
book.setParseAction(Book)


def rescan(b):
    pa_counter = ch_counter = sc_counter = 0
    container = None
    pa = None
    ch = None
    sc = None
    log.info('rescanning all tokens')
    count = 0
    toks = b.children
    b.children = []
    for tok in toks:
        log.debug(type(tok).__name__)
        if isinstance(tok, Scene):
            sc_counter += 1
            tok.__setattr__('seq', sc_counter)
            tok.__setattr__('ref', f's-{pa_counter}.{ch_counter:02}.{sc_counter:02}')
            tok.container = container
            if ch is not None:
                ch.children.append(tok)
                tok.parent = ch
            elif pa is not None:
                pa.children.append(tok)
                tok.parent = pa
            else:
                raise NotImplementedError('Scene with no container')
            sc = tok
        elif isinstance(tok, Sep):
            log.debug(f'-----{tok.render_field("url")}')
            c = tok.render_field('clazz')
            if c == 'end':
                sc = ch = pa = None
            elif c == 'file':
                container = tok
            elif c == 'stars':
                sc = None
            if ch is not None:
                ch.children.append(tok)
                tok.parent = ch
            elif pa is not None:
                pa.children.append(tok)
                tok.parent = pa
            else:
                b.children.append(tok)
                tok.parent = b
        elif isinstance(tok, Chapter):
            sc_counter = 0
            ch_counter += 1
            tok.__setattr__('seq', ch_counter)
            tok.__setattr__('ref', f'c-{pa_counter}.{ch_counter:02}.{sc_counter:02}')
            log.debug(f'-----{tok.title.render()}')
            tok.container = container
            if pa is not None:
                pa.children.append(tok)
                tok.parent = pa
            else:
                b.children.append(tok)
                tok.parent = b
            sc = None
            ch = tok
        elif isinstance(tok, Quot):
            t = tok.clazz.render() if tok.clazz else 'none'
            log.debug(f'-----{t}')
            tok.container = container
            if sc is not None:
                sc.children.append(tok)
                tok.parent = sc
            elif ch is not None:
                ch.children.append(tok)
                tok.parent = ch
            elif pa is not None:
                pa.children.append(tok)
                tok.parent = pa
            else:
                raise NotImplementedError('Fragment with no container')
        elif isinstance(tok, Part):
            sc_counter = 0
            ch_counter = 0
            pa_counter += 1
            tok.__setattr__('seq', pa_counter)
            tok.__setattr__('ref', f'p-{pa_counter}.{ch_counter:02}.{sc_counter:02}')
            log.debug(f'-----{tok.title.render()}')
            tok.container = container
            b.children.append(tok)
            tok.parent = b
            sc = None
            ch = None
            pa = tok
        else:
            tok.container = container
            if sc is not None:
                sc.children.append(tok)
                tok.parent = sc
            elif ch is not None:
                ch.children.append(tok)
                tok.parent = ch
            elif pa is not None:
                pa.children.append(tok)
                tok.parent = pa
            else:
                raise NotImplementedError('Fragment with no container')
        count += 1
    log.info(f'rescanning done ({count} tokens re-parsed)')
    return b


def parse(file, debug=False):
    if debug:
        log.setLevel(debug)

    log.info('parsing...')
    if isinstance(file, str):
        r = book.parseString(file, parseAll=True)
    else:
        r = book.parseFile(file, parseAll=True)

    r = rescan(r[0])
    return r
