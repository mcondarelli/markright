from .emitter import Emitter
import logging
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


class HTMLemitter(Emitter):

    def content(self, node, **kwargs):
        self.recurse(node, **kwargs)

    def para(self, node, **kwargs):
        fo = kwargs['__fo']
        fo.write(f'<p>')
        self.recurse(node, **kwargs)
        fo.write('</p>\n')

    def newline(self, node, **kwargs):
        if not node.para:
            fo = kwargs['__fo']
            fo.write('<br/>\n')

    def text(self, node, **kwargs):
        fo = kwargs['__fo']
        fo.write(node.text)

    # divisions ========================

    def book(self, node, **kwargs):
        fo = kwargs['__fo']
        fo.write("""\
<html>
<head>
<style>
.stars {
    text-align: center;
}
</style>
</head>
<body>
""")
        self.recurse(node, **kwargs)
        fo.write("""
</body>
</html>
""")

    def part(self, node, **kwargs):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '{} - ')
        fo = kwargs['__fo']
        fo.write(f'<h1{c}{i}>{n}{t}</h1>\n')
        self.recurse(node, **kwargs)
        fo.write('\n')

    def chapter(self, node, **kwargs):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '{} - ')
        fo = kwargs['__fo']
        fo.write(f'<h2{c}{i}>{n}{t}</h2>\n')
        self.recurse(node, **kwargs)
        fo.write('\n')

    def scene(self, node, **kwargs):
        c = node.render_field('clazz', ' class="{}"')
        t = node.render_field('title', '<!-- title: {} -->')
        i = node.render_field('ref', ' id="{}"')
        n = node.render_field('seq', '<!-- sequence: {} -->')
        fo = kwargs['__fo']
        fo.write(f'<div{c}{i}>{n}{t}\n')
        self.recurse(node, **kwargs)
        fo.write(f'</div>\n')

    def sep(self, node, **kwargs):
        c = node.render_field('clazz')
        t = node.render_field('url')
        fo = kwargs['__fo']
        if c == 'stars':
            fo.write(f'<p class="{c}"><img alt="* * *" class="{c}" src="{t}"/></p>')
        else:
            fo.write(f'<!-- {c}:{t} -->\n')

    # COMMANDS =========================

    def speech(self, node, **kwargs):
        # TODO: implement speeech nesting
        clazz = node.render_field('clazz', ' class="{}"')
        fo = kwargs['__fo']
        fo.write(f'« <span{clazz}>')
        self.recurse(node, **kwargs)
        fo.write('</span> »')

    def stress(self, node, **kwargs):
        clazz = node.render_field('clazz', ' class="{}"')
        fo = kwargs['__fo']
        fo.write(f'<b{clazz}>')
        self.recurse(node, **kwargs)
        fo.write('</b>')

    def standout(self, node, **kwargs):
        clazz = node.render_field('clazz', ' class="{}"')
        fo = kwargs['__fo']
        fo.write(f'<i{clazz}>')
        self.recurse(node, **kwargs)
        fo.write('</i>')

    def quot(self, node, **kwargs):
        # TODO: see if we need to add something to .css
        clazz = node.render_field('clazz', ' class="{}"')
        style = node.render_field('style', ' style="{}"')
        fo = kwargs['__fo']
        fo.write(f'<i{clazz}{style}>')
        self.recurse(node, **kwargs)
        fo.write('</i>')

    def img(self, node, **kwargs):
        clazz = node.render_field('clazz', ' class="{}"')
        url = node.render_field('url', ' src="{}"')
        fo = kwargs['__fo']
        fo.write(f'<img{clazz}{url}/>')

    def div(self, node, **kwargs):
        clazz = node.render_field('clazz', ' class="{}"')
        style = node.render_field('style', ' style="{}"')
        fo = kwargs['__fo']
        fo.write(f'<div{clazz}{style}>')
        self.recurse(node, **kwargs)
        fo.write('</div>')

    # DRIVER ===========================

    def to_html(self, node, **kwargs):
        fname = kwargs.get('fname', 'out.html')
        with open(fname, 'w') as fo:
            kwargs['__fo'] = fo
            self.emit(node, **kwargs)
