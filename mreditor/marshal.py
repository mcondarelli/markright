from string import punctuation

from PyQt5 import QtGui, QtCore

from markright import emitter

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

__all__ = ['TreeEmitter', 'TreeSaver',
           'save_markright',
           'TextRole', 'DescRole', 'TitleRole',
           'ClassProperty']

TextRole = int(QtCore.Qt.UserRole + 0)
ClassRole = int(QtCore.Qt.UserRole + 1)
RefRole = int(QtCore.Qt.UserRole + 2)
DescRole = int(QtCore.Qt.UserRole + 3)
TitleRole = int(QtCore.Qt.UserRole + 4)
MetaRole = int(QtCore.Qt.UserRole + 5)

# TODO: parametrize all @ref[whatever] to allow easy addition. Now it must be changed in too many places.

ClassProperty = int(QtGui.QTextFormat.UserProperty + 0)

speech_head = '«\u202f'
speech_tail = '\u202f»'


class TreeEmitter(emitter.Emitter):
    def __init__(self):
        super(TreeEmitter, self).__init__()
        self.root = None
        self.space = ''
        self.doc = None
        self.fmt = None
        self.cur = None
        self.par = None
        self.cls = None

    def book(self, node, **kwargs):
        setattr(node, 'fe_item', self.root)
        self.space = ''
        self.recurse(node, **kwargs)

    def part(self, node, **kwargs):
        parent = node.parent.fe_item
        t = node.render_field('title')
        n = node.render_field('seq')
        i = QtGui.QIcon(':/icon/icons/part.svg')
        item = QtGui.QStandardItem(i, f'{n:2} - {t}')
        parent.appendRow(item)
        setattr(node, 'fe_item', item)

        c = node.render_field('clazz')
        t = node.render_field('title')
        i = node.render_field('ref')
        item.setData(c, ClassRole)
        item.setData(t, TitleRole)
        item.setData(i, RefRole)

        self.space = ''
        self.recurse(node, **kwargs)

    def chapter(self, node, **kwargs):
        parent = node.parent.fe_item
        t = node.render_field('title')
        n = node.render_field('seq')
        i = QtGui.QIcon(':/icon/icons/chapter.svg')
        item = QtGui.QStandardItem(i, f'{n:2} - {t}')
        parent.appendRow(item)
        setattr(node, 'fe_item', item)

        c = node.render_field('clazz')
        t = node.render_field('title')
        i = node.render_field('ref')
        item.setData(c, ClassRole)
        item.setData(t, TitleRole)
        item.setData(i, RefRole)

        self.space = ''
        self.recurse(node, **kwargs)

    def scene(self, node, **kwargs):
        parent = node.parent.fe_item
        t = node.render_field('title')
        n = node.render_field('seq')
        i = QtGui.QIcon(':/icon/icons/scene.svg')
        item = QtGui.QStandardItem(i, f'{n:2} - {t}')
        parent.appendRow(item)
        setattr(node, 'fe_item', item)

        c = node.render_field('clazz')
        t = node.render_field('title')
        i = node.render_field('ref')
        item.setData(c, ClassRole)
        item.setData(t, TitleRole)
        item.setData(i, RefRole)

        doc = self.scene_to_doc(node)
        mr = TreeSaver().to_markright(doc)
        item.setData(mr, TextRole)

    def scene_to_doc(self, node):
        self.doc = QtGui.QTextDocument()
        self.fmt = QtGui.QTextCharFormat()
        self.cur = QtGui.QTextCursor(self.doc)
        self.par = ''
        self.cls = []
        self.recurse(node)
        self.flush()
        return self.doc

    def sep(self, node, **kwargs):
        pass  # TODO: implement

    def meta(self, node, **kwargs):
        parent = node.parent.fe_item
        c = node.render_field('clazz')

        save_doc = self.doc
        save_fmt = self.fmt
        save_cur = self.cur

        self.doc = QtGui.QTextDocument()
        self.fmt = QtGui.QTextCharFormat()
        self.cur = QtGui.QTextCursor(self.doc)
        self.par = ''
        self.cls = []
        self.recurse(node)
        self.flush()
        s = self.doc.toPlainText()

        self.cur = save_cur
        self.fmt = save_fmt
        self.doc = save_doc

        if c == 'desc':
            parent.setData(s, DescRole)
        elif c == 'ref':
            parent.setData(s, RefRole)
        else:
            # future-proof: generic meta tag
            meta = parent.data(MetaRole)
            if meta is None:
                meta = {}
            meta[c] = s
            parent.setData(meta, MetaRole)

    def flush(self):
        if self.par:
            self.cur.insertText(self.par, self.fmt)
            self.par = ''

    # COMMANDS =========================

    def _command(self, node, cls='', head='', tail=''):
        clazz = node.render_field('clazz')
        if not clazz:
            clazz = cls

        save_cls = list(self.cls)
        for c in clazz.split():
            if c not in self.cls:
                self.cls.append(c)
                self.flush()

        self.fmt.setProperty(ClassProperty, ' '.join(self.cls))
        self.par += head
        self.recurse(node)
        self.par += tail
        self.flush()
        self.cls = save_cls
        self.fmt.setProperty(ClassProperty, ' '.join(self.cls))

    def speech(self, node, **kwargs):
        self.flush()
        # TODO: implement speech nesting
        fmt = QtGui.QTextCharFormat(self.fmt)
        col = QtGui.QColor()
        col.setNamedColor('navy')
        self.fmt.setForeground(col)
        self.space = ''
        self._command(node, 'speech', head=speech_head, tail=speech_tail)
        self.space = ' '
        self.fmt = fmt

    def stress(self, node, **kwargs):
        self.flush()

        fmt = QtGui.QTextCharFormat(self.fmt)
        self.fmt.setFontItalic(True)
        self._command(node, 'stress')
        self.space = ' '
        self.fmt = fmt

    def standout(self, node, **kwargs):
        self.flush()
        fmt = QtGui.QTextCharFormat(self.fmt)
        self.fmt.setFontWeight(QtGui.QFont.Bold)
        self._command(node, 'standout')
        self.space = ' '
        self.fmt = fmt

    def quot(self, node, **kwargs):
        self.flush()
        fmt = QtGui.QTextCharFormat(self.fmt)
        self.fmt.setFontItalic(True)
        col = QtGui.QColor()
        col.setNamedColor('darkolivegreen')
        self.fmt.setForeground(col)
        self._command(node, 'quote')
        self.space = ' '
        self.fmt = fmt

    def img(self, node, **kwargs):
        pass

    def div(self, node, **kwargs):
        # TODO: implement properly
        clazz = node.render_field('clazz', ' class="{}"')
        style = node.render_field('style', ' style="{}"')
        fo = kwargs['__fo']
        fo.write(f'<div{clazz}{style}>')
        self.recurse(node, **kwargs)
        fo.write('</div>')

    # TEXT =============================

    def content(self, node, **kwargs):
        self.recurse(node, **kwargs)

    def para(self, node, **kwargs):
        self.flush()
        if not self.cur.atStart():
            self.cur.insertBlock()
        self.space = ''
        self.recurse(node, **kwargs)

    def newline(self, node, **kwargs):
        if not node.para:
            self.par += '\n'
            self.space = ''

    def text(self, node, **kwargs):
        if node.text[0] not in punctuation:
            self.par += self.space
            self.space = ''
        self.par += node.text

    # DRIVER ===========================

    def to_tree(self, node, **kwargs):
        model = kwargs.get('model', QtGui.QStandardItemModel())
        self.root = model.invisibleRootItem()
        self.emit(node, **kwargs)
        return model

    def src_to_doc(self, src):
        from markright.book_parser import book
        r = book.parseString(src.strip(), True)
        doc = self.scene_to_doc(r[0])
        return doc


def save_markright(model: QtGui.QStandardItemModel, fname: str):

    with open(fname, 'w') as fo:

        # iterate over parts
        root = QtCore.QModelIndex()
        for r in range(model.rowCount(root)):
            pi = model.index(r, 0, root)
            # emit part data
            fo.write('\n@part')
            clazz = model.data(pi, ClassRole)
            if clazz:
                fo.write('[' + clazz + ']')
            title = model.data(pi, TitleRole)
            if title is None:
                title = 'Untitled'
            fo.write('{' + title + '}\n')
            desc = model.data(pi, DescRole)
            if desc:
                fo.write('@meta[desc]{' + desc + '}\n')
            ref = model.data(pi, RefRole)
            if ref:
                fo.write('@meta[ref]{' + desc + '}\n')
            text = model.data(pi, TextRole)
            if text:
                fo.write(text)
                fo.write('\n')

            # iterate over chapters
            for c in range(model.rowCount(pi)):
                ci = model.index(c, 0, pi)
                # emit chapter data
                fo.write('\n@chapter')
                clazz = model.data(ci, ClassRole)
                if clazz:
                    fo.write('[' + clazz + ']')
                title = model.data(ci, TitleRole)
                if title is None:
                    title = 'Untitled'
                fo.write('{' + title + '}\n')
                desc = model.data(ci, DescRole)
                if desc:
                    fo.write('@meta[desc]{' + desc + '}\n')
                ref = model.data(pi, RefRole)
                if ref:
                    fo.write('@meta[ref]{' + desc + '}\n')
                text = model.data(ci, TextRole)
                if text:
                    fo.write(text)
                    fo.write('\n')

                # iterate over scenes
                for s in range(model.rowCount(ci)):
                    si = model.index(s, 0, ci)
                    # emit chapter data
                    fo.write('\n@scene')
                    clazz = model.data(si, ClassRole)
                    if clazz:
                        fo.write('[' + clazz + ']')
                    title = model.data(si, TitleRole)
                    if title is None:
                        title = 'Untitled'
                    fo.write('{' + title + '}\n')
                    desc = model.data(si, DescRole)
                    if desc:
                        fo.write('@meta[desc]{' + desc + '}\n')
                    ref = model.data(pi, RefRole)
                    if ref:
                        fo.write('@meta[ref]{' + desc + '}\n')
                    text = model.data(si, TextRole)
                    if text:
                        fo.write(text.strip())
                        fo.write('\n\n')


class TreeSaver(object):

    def __init__(self):
        self.span = None
        self.out = None

    def start(self):
        self.span = ''
        self.out = ''

    def append(self, txt):
        self.span += txt

    def open(self, clss, txt):
        if 'speech' in clss:
            self.append(f'@speech[{" ".join(clss)}]{{')
            # remove head and tail TODO: parametrize
            if txt.startswith(speech_head):
                txt = txt[len(speech_head):]
        elif 'standout' in clss:
            self.append(f'@standout[{" ".join(clss)}]{{')
        elif 'stress' in clss:
            self.append(f'@stress[{" ".join(clss)}]{{')
        elif 'quot' in clss:
            self.append(f'@quot[{" ".join(clss)}]{{')
        return txt

    def close(self, clss):
        if 'speech' in clss:
            # remove head and tail TODO: parametrize
            if self.span.endswith(speech_tail):
                self.span = self.span[:-len(speech_tail)]
            self.append('}')
        elif 'standout' in clss:
            self.append('}')
        elif 'stress' in clss:
            self.append('}')
        elif 'quot' in clss:
            self.append('}')

    def transfer(self):
        self.out += self.span
        self.out += '\n\n'
        self.span = ''

    def end(self):
        out = self.out
        self.span = None
        self.out = None
        return out

    def to_markright(self, doc: QtGui.QTextDocument,
                     begin: QtGui.QTextCursor = None,
                     end: QtGui.QTextCursor = None):
        self.start()
        bi = doc.firstBlock() if begin is None else begin.block()
        while bi.isValid():
            csa = frozenset()
            fi = bi.begin()
            while not fi.atEnd():
                fr = fi.fragment()
                if fr.isValid():
                    txt = fr.text()
                    fmt = fr.charFormat()
                    cls = fmt.property(ClassProperty)
                    csb = frozenset(cls.split() if cls is not None else [])
                    s = csa - csb  # classes closed
                    self.close(s)
                    s = csb - csa  # classes opening
                    txt = self.open(s, txt)
                    self.append(txt)
                    # log.debug(f'[{cls if cls is not None else "NONE":30}] "{fr.text()}')
                    csa = csb
                fi += 1
            self.close(csa)
            self.transfer()
            bi = bi.next()
            if end and bi == end.block():
                break
        out = self.end()
        return out


def start(book):
    log.info('building tree...')
    model = TreeEmitter().to_tree(book, model=QtGui.QStandardItemModel())
    log.info('dumping tree...')
    save_markright(model, 'out.markright')
    log.info('done.')
