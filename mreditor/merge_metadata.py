import xml.etree.ElementTree as ET
from PyQt5 import QtGui, QtCore

from marshal import save_markright
from .marshal import *

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


def start(book, src, out):

    log.info('reading source...')
    doc = ET.parse(src)
    bks = doc.getroot().findall('book')

    log.info('building tree...')
    model = QtGui.QStandardItemModel()
    TreeEmitter(model).to_tree(book)

    log.info('merging info...')
    # iterate over parts
    root = QtCore.QModelIndex()
    for r in range(model.rowCount(root)):
        pi = model.index(r, 0, root)
        info = model.data(pi, InfoRole)
        log.info('--- %s', info['title'])
        bk = bks[r]
        chs = bk.findall('chapter')
        info['desc'] = bk.attrib.get('desc')
        model.setData(pi, info, InfoRole)

        # iterate over chapters
        for c in range(model.rowCount(pi)):
            ci = model.index(c, 0, pi)
            info = model.data(ci, InfoRole)
            log.info('------ %s', info['title'])
            ch = chs[c]
            scs = ch.findall('scene')
            info['desc'] = ch.attrib.get('desc')
            model.setData(ci, info, InfoRole)

            # iterate over scenes
            for s in range(model.rowCount(ci)):
                si = model.index(s, 0, ci)
                info = model.data(si, InfoRole)
                sc = scs[s]
                log.info('--------- %s -- %s', info['seq'], sc.attrib.get('title'))
                if not info.get('title'):
                    n = info['seq']
                    t = sc.attrib.get('title')
                    model.itemFromIndex(si).setText(f'{n:2} - {t}')
                    info['title'] = t
                info['desc'] = sc.attrib.get('desc')
                model.setData(si, info, InfoRole)

    log.info('dumping tree...')
    save_markright('out.markright')
    log.info('done.')
