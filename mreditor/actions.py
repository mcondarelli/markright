from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from os import path
import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


base = 'icons'
theme = 'Default'


def get_icon(name):
    p = base
    if theme:
        p = path.join(p, theme)
    p = path.join(p, name + '.svg')
    # TODO: search for tight format
    return QIcon(p)


class ActionBold(QAction):
    def __init__(self, parent: QWidget, target: QTextEdit):
        super(ActionBold, self).__init__(get_icon("font-bold"), "Bold", parent)
        self.setCheckable(True)
        self.cursor = target.textCursor()
        t = self.cursor.selection().toHtml()
        log.debug(t)
        self.char_format = self.cursor.charFormat()
        self.weight = self.char_format.fontWeight()
        self.setChecked(self.weight == QFont.Bold)
        self.triggered.connect(self.doit)

    def doit(self):
        w = QFont.Bold if self.isChecked() else QFont.Normal
        self.char_format.setFontWeight(w)
        self.cursor.setCharFormat(self.char_format)


class ActionItalic(QAction):
    def __init__(self, parent: QWidget, target: QTextEdit):
        super(ActionItalic, self).__init__(get_icon("font-italic"), "Italic", parent)
        self.setCheckable(True)
        self.cursor = target.textCursor()
        self.char_format = self.cursor.charFormat()
        self.italic = self.char_format.fontItalic()
        self.setChecked(self.italic)
        self.triggered.connect(self.doit)

    def doit(self):
        self.char_format.setFontItalic(self.isChecked())
        self.cursor.setCharFormat(self.char_format)


class ActionFont(QWidgetAction):
    def __init__(self, parent: QWidget, target: QTextEdit):
        super(ActionFont, self).__init__(parent)
        self.setIcon(get_icon("font-face"))
        self.setText("Face")
        w = QFontComboBox()
        w.currentFontChanged.connect(self.doit)
        self.setDefaultWidget(w)
        self.cursor = target.textCursor()
        self.char_format = self.cursor.charFormat()
        font = self.char_format.font()
        w.setCurrentFont(font)
        # self.triggered.connect(self.doit)

    def doit(self, font):
        self.char_format.setFont(font)
        self.cursor.setCharFormat(self.char_format)


class ActionSize(QWidgetAction):
    def __init__(self, parent: QWidget, target: QTextEdit):
        super(ActionSize, self).__init__(parent)
        self.setIcon(get_icon("font-size"))
        self.setText("Size")
        self.has_changed = False
        w = QSpinBox()
        self.setDefaultWidget(w)
        self.cursor = target.textCursor()
        self.char_format = self.cursor.charFormat()
        font = self.char_format.font()
        size = font.pointSize()
        w.setRange(6, 100)
        w.setValue(size)
        w.valueChanged.connect(self.doit)
        w.editingFinished.connect(self.quit)

    def doit(self, size):
        print(f'ActionSize.doit({size})')
        self.char_format.setFontPointSize(size)
        self.cursor.setCharFormat(self.char_format)
        self.has_changed = True

    def quit(self):
        print(f'ActionSize.quit()')
        if self.has_changed:
            print(f'ActionSize.quit(quitting)')
