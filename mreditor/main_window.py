from os import path
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from .marshal import *

from . import icons
# TODO: add "help -> about" menu entry with all attributions needed (images, icons, ...)

import logging
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)


class StatusbarLogger(logging.Handler):
    colors = {
        'DEBUG': 'grey',
        'INFO': 'forestgreen',
        'WARNING': 'orange',
        'ERROR': 'orangered',
        'CRITICAL': 'crimson'
    }

    def __init__(self, sb: QtWidgets.QStatusBar):
        super(StatusbarLogger, self).__init__()
        self.sb = sb

    def emit(self, record):
        msg = self.format(record)
        self.sb.showMessage(msg, 2000)
        c = self.colors.get(record.levelname, 'purple')
        self.sb.setStyleSheet(f'color: {c}')


class MWin(QtWidgets.QMainWindow):
    te: QtWidgets.QTextEdit
    tv: QtWidgets.QTreeView
    css = '''\
p {
    font-family: serif;
    margin-top: -0.6em;
    text-align: justify;
}
.speech {
    color: olive;
}
.god-speech {
    color: goldenrod;
    font-family: sans-serif;
}
    '''

    def __init__(self, out):
        super(MWin, self).__init__()
        self.out = out
        self.model = None
        self.dirty = False
        self.status = QtWidgets.QLabel()

    def setup(self, node):
        # load model ======================================
        self.model = TreeEmitter().to_tree(node)
        self.model.setParent(self)
        self.tree.setModel(self.model)
        self.tree.expandAll()
        self.dirty = False

        # setup actions ===================================
        bl = self.centralWidget().findChildren(QtWidgets.QToolButton)
        for b in bl:
            a = getattr(self, 'action_'+b.objectName(), None)
            if a is not None and isinstance(a, QtWidgets.QAction):
                log.debug(f'QToolButton({b.objectName()}) -> {a.objectName()}')
                b.setDefaultAction(a)

        # redirect logging ===============================
        log.addHandler(StatusbarLogger(self.statusbar))

        # statusbar setup ================================
        self.statusbar.addPermanentWidget(self.status)
        self.status.setVisible(False)
        self.edit.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def maybe_save(self):
        if self.dirty:
            self.save()

    def save(self):
        out = self.out
        if out is None:
            out = QtWidgets.QFileDialog.getSaveFileName(self, 'Save file',
                                                        filter='Markright files (*.markright);;'
                                                               'All files(*.*)')[0]
        if out:
            save_markright(self.model, out)
            self.dirty = False

    @QtCore.pyqtSlot()
    def on_action_save_prev_triggered(self):
        log.debug('on_action_save_prev_triggered()')
        self.on_save_scene_activated()
        self.on_action_discard_prev_triggered()

    @QtCore.pyqtSlot()
    def on_action_discard_prev_triggered(self):
        log.debug('on_action_discard_prev_triggered()')
        prv = self.tree.indexAbove(self.tree.currentIndex())
        if prv is not None and prv.isValid():
            self.select(prv)

    @QtCore.pyqtSlot()
    def on_action_discard_next_triggered(self):
        log.debug('on_action_discard_next_triggered()')
        nxt = self.tree.indexBelow(self.tree.currentIndex())
        if nxt is not None and nxt.isValid():
            self.select(nxt)

    @QtCore.pyqtSlot()
    def on_action_save_next_triggered(self):
        log.debug('on_action_save_next_triggered()')
        self.on_save_scene_activated()
        self.on_action_discard_next_triggered()

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_tree_activated(self, idx):
        log.debug('on_tree_activated(%d)', idx.row())
        self.select(idx)

    @QtCore.pyqtSlot(QtCore.QModelIndex)
    def on_tree_clicked(self, idx):
        if self.edit.document().isModified():
            r = QtWidgets.QMessageBox.question(self,
                                               'Warning: Scene was modified',
                                               'Do you want to save?',
                                               QtWidgets.QMessageBox.Save | QtWidgets.QMessageBox.No,
                                               QtWidgets.QMessageBox.Save)
            if r == QtWidgets.QMessageBox.Save:
                self.on_save_scene_activated()
        self.select(idx)

    def on_save_scene_activated(self):
        if self.edit.document().isModified():
            doc = self.edit.document()
            idx = self.tree.currentIndex()
            src = TreeSaver().to_markright(doc)
            self.model.setData(idx, src, TextRole)
            doc.setModified(False)
            self.dirty = True

    def select(self, idx: QtCore.QModelIndex):
        log.debug('select(%d)', idx.row())
        self.edit_info.setChecked(False)
        src = self.model.data(idx, TextRole)
        if src:
            doc = TreeEmitter().src_to_doc(src)
            doc.setModified(False)
            self.edit.setDocument(doc)
            self.edit.setReadOnly(False)
        else:
            self.edit.clear()
            self.edit.setReadOnly(True)
            self.tree.expand(idx)
        self.tree.setCurrentIndex(idx)
        s = self.model.data(idx, TitleRole)
        self.title.setText(s)
        s = self.model.data(idx, DescRole)
        self.desc.setPlainText(s)

    def on_title_textChanged(self, new):
        item = self.model.itemFromIndex(self.tree.currentIndex())
        item.setData(new, TitleRole)
        item.setText(f'{item.row()+1:2} - {new}')
        self.dirty = True

    def on_desc_textChanged(self):
        text = self.desc.toPlainText()
        item = self.model.itemFromIndex(self.tree.currentIndex())
        item.setData(text, DescRole)
        self.dirty = True

    @QtCore.pyqtSlot()
    def on_action_cut_scene_triggered(self):
        log.debug('on_action_cut_scene_triggered()')
        item = self.model.itemFromIndex(self.tree.currentIndex())
        desc = item.data(DescRole)
        n = item.row() + 1                      # new scene item position (after current)
        new = QtGui.QStandardItem('')           # crete new item
        new.setData(desc, DescRole)             # duplicate description
        new.setData('Untitled', TitleRole)      # fake title
        parent = item.parent()
        parent.insertRow(n, new)                # insert new item in the right place.
        for i in range(parent.rowCount()):      # renumber scenes (could be done just from next onward)
            it = parent.child(i)
            ti = it.data(TitleRole)
            it.setText(f'{i+1:2} - {ti}')

        # do actual splitting
        # we do it manually because cur'n'paste and similar methods do not preserve our data.
        doc = self.edit.document()

        # insert newline to force block splitting.
        cursor = self.edit.textCursor()
        # cursor.insertText('\n')
        # if not cursor.atBlockStart():
        #     raise ValueError('Cursor not at block start')

        src = TreeSaver().to_markright(doc, end=cursor)
        item.setData(src, TextRole)
        src = TreeSaver().to_markright(doc, begin=cursor)
        new.setData(src, TextRole)

        self.select(self.model.indexFromItem(new))
        self.info.setChecked(True)
        self.title.setFocus()
        self.dirty = True

    # @QtCore.pyqtSlot()
    # def on_edit_cursorPositionChanged():
    #     cursor = self.edit.getCursor()

    @QtCore.pyqtSlot(QtGui.QTextCharFormat)
    def on_edit_currentCharFormatChanged(self, fmt):
        usr = fmt.property(ClassProperty)
        if usr:
            self.status.setText(usr)
            self.status.setVisible(True)
        else:
            self.status.setVisible(False)

    @QtCore.pyqtSlot(QtCore.QPoint)
    def on_edit_customContextMenuRequested(self, pos):
        class ActionTag(QtWidgets.QAction):
            def __init__(self, tag: str, parent: QtWidgets.QWidget, target: QtWidgets.QTextEdit):
                super(ActionTag, self).__init__(tag, parent)
                self.target = target
                self.triggered.connect(self.doit)

            def doit(self):
                tag = self.text()
                cursor = self.target.textCursor()
                fmt = cursor.charFormat()
                tags = fmt.property(ClassProperty)
                if tags:
                    if tag not in tags:
                        tags += ' ' + tag
                else:
                    tags = tag
                fmt.setProperty(ClassProperty, tags)
                cursor.mergeCharFormat(fmt)

        # cursor = self.text.textCursor()
        menu: QtWidgets.QMenu = self.edit.createStandardContextMenu(pos)
        submenu = QtWidgets.QMenu("Tags", menu)
        submenu.addAction(ActionTag('speech', self, self.edit))
        submenu.addAction(ActionTag('standout', self, self.edit))
        submenu.addAction(ActionTag('stress', self, self.edit))
        submenu.addAction(ActionTag('actor', self, self.edit))
        menu.addMenu(submenu)
        menu.move(self.edit.mapToGlobal(pos))
        menu.exec()

    @QtCore.pyqtSlot(bool)
    def on_edit_info_toggled(self, on):
        self.title.setReadOnly(not on)
        self.desc.setReadOnly(not on)

    @QtCore.pyqtSlot()
    def on_actionAbout_triggered(self):
        QtWidgets.QMessageBox.about(self, 'About mreditor',
                                    '''\
Qt editor for Markright files.

Some icons are courtesy of (add attributions).
''')

    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        self.maybe_save()


def start(book, out):
    app = QtWidgets.QApplication([])
    mw = MWin(out)
    uifn = path.join(path.dirname(__file__), 'main_window.ui')
    uic.loadUi(uifn, mw)
    mw.setup(book)
    mw.show()
    app.exec()
