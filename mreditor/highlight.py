from PyQt5.QtGui import QSyntaxHighlighter
from pyparsing import *


argument = '[' + ZeroOrMore(Word(alphas, alphanums+'-_')) + ']'
fragment = Forward()
content = '{' + Optional(fragment) + '}'
command = Word('@', alphas) + Optional(argument + Optional(argument)) + content
fragment <<= OneOrMore(command | Regex(r'[^@}]+'))

#

lbracket = Literal('[').suppress()
rbracket = Literal(']').suppress()
lbrace = Literal('{').suppress()
rbrace = Literal('}').suppress()

clazz = lbracket + ZeroOrMore(Word(alphas+'-')) + rbracket
url = lbrace + Combine(OneOrMore(Word(alphanums+'-_.') | '/' | ':')) + rbrace

title = lbrace + ZeroOrMore(Word(alphanums+'-_.')) + rbrace
text = Regex(r'[^@}\n]+')
style = lbracket + ZeroOrMore((Word(alphas+'-') + ':' + Word(printables+' ', excludeChars=';') + ';')) + rbracket

frag = Forward()

sep = Literal('@sep') + Optional(clazz) + Optional(url)
img = Literal('@img') + Optional(clazz) + url
div = Literal('@div') + clazz + Optional(style) + lbrace + frag + rbrace


span = Literal('@span') + clazz + lbrace + frag + rbrace
part = Literal("@part") + Optional(clazz) + title
chapter = Literal("@chapter") + Optional(clazz) + title
scene = Literal("@scene") + Optional(clazz) + title

speech = Literal("@speech") + Optional(clazz) + lbrace + frag + rbrace
stress = Literal("@stress") + Optional(clazz) + lbrace + frag + rbrace
standout = Literal("@standout") + Optional(clazz) + lbrace + frag + rbrace
quot = "@quot" + Optional(clazz) + Optional(style) + lbrace + frag + rbrace

frag <<= OneOrMore(div
                   | img
                   | speech
                   | stress
                   | standout
                   | span
                   | text)


def reparse(s, loc, toks):
    if toks[0] == '{' and toks[-1] == '}':
        inn = ' '.join(toks[1:-2])
        for sub in cmd.searchString(inn):
            print('----->', found.asList())


tag = Word('@', alphas)
cnt = Forward()
cnt.setParseAction(reparse)
cmd = tag + (clazz + style + title | clazz + title | title |
             clazz + style + url | clazz + url | url |
             clazz + style + cnt | clazz + cnt | cnt)
txt = Regex(r'[^]@}]+')
cnt <<= '{' + OneOrMore(cmd | txt) + '}'

# openbrace = Literal('{')
# text = Word(alphas + alphas8bit)
# closebrace = Literal('}')
#
# paragraph = Forward()
# paraitam = text | paragraph
# paragraph <<= tag + openbrace + ZeroOrMore(paraitam) + closebrace


class MarkrightHighlighter(QSyntaxHighlighter):

    def __init__(self, doc):
        super(MarkrightHighlighter, self).__init__(doc)

    def highlightBlock(self, text: str) -> None:
        pass


if __name__ == '__main__':
    # with open('../../Cronache_della_Nuova_Terra.markright') as fi:
    #     data = fi.read()
    data = '''\
@sep{OEBPS/Text/1-In_cerca_di_un_Mondo_Nuovo.html}

@part{1 In cerca di un Mondo Nuovo}
@div[][text-align: center; ]{@img[]{../Images/old_mage.jpg}
}
@sep[file]{OEBPS/Text/1.01-La_Missione.html}

@chapter{1 La Missione}
@scene{}
La Pianta non era certo imponente, somigliava a una verza troppo cresciuta, alta quasi un metro e mezzo e larga altrettanto,

Era proprio la zucca a occupare per intero l’attenzione dell’Assistente che, immobile, la osservava.

Si trattava di un frutto oblungo con la buccia di un bel colore giallo solcato da striature più chiare. Ora si stava agitand

Qualche minuto dopo, d’improvviso, la zucca si lacerò esponendo alla luce del sole il suo contenuto.
L’Assistente scattò in avanti, raccolse il corpicino, recise il cordone ombelicale e, dopo una rapida occhiata per accertars
Il piccolo Elfo lanciò il suo primo vagito.

L’Assistente raccolse anche i resti del frutto con la placenta ancora attaccata e si diresse a passo spedito verso la bassa
Qualcun altro sarebbe presto venuto a prendersi cura della Pianta..

@sep[stars]{../Images/sep.svg}


@scene{}
'''

    test = r"""\
Starting text @emph{This sentence is in @textit{italics} in Bembo}
@emph{This sentence is in @textit{italics} in bembo and in @emph{Italian}}
Middle filling @emph{This second sentence is in Emphasis}
End"""

    #result = book.parseString(data)
    result = cmd.searchString(data)
    for found in result:
        print('-->', found.asList())
